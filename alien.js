var alienRouter = require('express').Router()
var _ = require('lodash')

var aliens = []
var id = 0

var updateId = function (req, res, next) {
  if (!req.body.id) {
    id++
    req.body.id = id + ''
  }
  next()
}

alienRouter.param('id', function (req, res, next, id) {
  var alien = _.find(aliens, { id: id })
  if (alien) {
    req.alien = alien
    next()
  } else {
    res.send()
  }
})

alienRouter.route('/')
  .get(function (req, res) {
    res.json(aliens)
  })
  .post(updateId, function (req, res) {
    var alien = req.body

    aliens.push(alien)

    res.json(alien)
  })

alienRouter.route('/:id')
  .get(function (req, res) {
    var alien = req.alien
    res.json(alien || {})
  })
  .delete(function (req, res) {
    var alien = _.findIndex(aliens, { id: req.params.id })
    aliens.splice(alien, 1)
    res.json(req.alien)
  })
  .put(function (req, res) {
    var update = req.body
    if (update.id) {
      delete update.id
    }
  })

alienRouter.put('/:id', function (req, res) {
  var update = req.body
  if (update.id) {
    delete update.id
  }

  var alien = _.findIndex(aliens, { id: req.params.id })
  if (!aliens[alien]) {
    res.send()
  } else {
    var updatedAlien = _.assign(aliens[alien], update)
    res.json(updatedAlien)
  }
})

module.exports = alienRouter
