var express = require('express')
var bodyParser = require('body-parser')
var app = express()
var morgan = require('morgan')

var alienRouter = require('./alien')

app.use(morgan('dev'))
app.use(express.static('client'))

// body parser to post json to the server
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// Mounting, this router will be used when a req for '/alien'  comes through
app.use('/alien', alienRouter)

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/', function (err) {
    if (err) {
      res.status(500).send(err)
    }
  })
})

app.listen(3000)
console.log('Server is listening at 3000')
